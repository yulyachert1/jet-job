import Vue from "vue";
import VueRouter from "vue-router";

import Home from "../pages/Home.vue";
import Admin from "../pages/Admin.vue";
import Responses from "../pages/Responses.vue";
import Vacancies from "../pages/Vacancies.vue";

Vue.use(VueRouter);

export default new VueRouter({
  mode: "history",

  routes: [
    {
      path: `/`,
      name: "home",
      component: Home,
    },
    {
      path: `/admin`,
      name: "admin",
      component: Admin,
    },
    {
      path: `/responses`,
      name: "responses",
      component: Responses,
    },
    {
      path: `/vacancies`,
      name: "vacancies",
      component: Vacancies,
    },
  ],
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition;
    } else {
      return { x: 0, y: 0 };
    }
  },
});
